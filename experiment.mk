
PROJECTS =		\
	node		\
	zeromq2		\
	redis		\
	clojure		\
	voldemort	\

DATASET        = data/commits.dat
DATASET_SQL    = analysis/dataset.sql
PROJECTS_FILE  = data/projects.dat
DATASETS       = $(DATASET) $(PROJECTS_FILE)
DB_NAME        = authorship-study
DB_DBI         = dbi:Pg:dbname=$(DB_NAME)
PROJECTS_STAMP = $(patsubst %, data/%.stamp, $(PROJECTS))
PROJECTS_HEADS = $(patsubst %, rawdata/%/.git/refs/heads/master, $(PROJECTS))
ANALYSIS_STAMP = analysis/stamp
RESULTS_STAMP  = data/results.stamp
TIME           = /usr/bin/time
SQLITE         = sqlite3 -init /dev/null -separator " " -header -nullvalue NA
PSQL           = psql -A -q -F ' ' -P footer=off -P null=NA
LANG           = C

EXPERIMENT_LOG = log/$(shell date +%Y-%m-%d-%H:%M).log

DEPENDENCIES   = $(shell grep '^\s*[a-z]' dependencies.debian)

install-dependencies-debian:
	sudo apt-get install $(DEPENDENCIES)

experiment-run:
	$(MAKE) --no-print-directory $(DATASETS) 2>&1 | tee $(EXPERIMENT_LOG)

$(DATASET) $(PROJECTS_FILE): $(RESULTS_STAMP)

$(RESULTS_STAMP): $(ANALYSIS_STAMP) $(DATASET_SQL)
	$(TIME) --format '[sql] Processing time = %E' $(PSQL) $(DB_NAME) < $(DATASET_SQL) > $(DATASET)
	$(PSQL) $(DB_NAME) -c 'SELECT id, trim(name) as name FROM projects ORDER BY name' > /tmp/results.sql
	cp /tmp/results.sql $(PROJECTS_FILE)
	$(MAKE) --no-print-directory sha1sums
	touch $@

sha1sums:
	@echo '----------------------------------------------'
	@echo 'Git heads of the analyzed projects'
	@echo '----------------------------------------------'
	@echo
	@for head in $(PROJECTS_HEADS); do echo "$$(cat $$head)  $$head"; done
	@echo
	@echo '----------------------------------------------'
	@echo 'SHA1 checksums of intermediary data files,'
	@echo 'output files and local code'
	@echo '----------------------------------------------'
	@echo
	@sha1sum $(DATASET) $(PROJECTS_FILE)
	@sha1sum analysis/process $(DATASET_SQL) Makefile
	@echo
	@echo '----------------------------------------------'
	@echo 'Analizo version'
	@echo '----------------------------------------------'
	@echo
	@analizo --version
	@echo
	@echo '----------------------------------------------'
	@echo 'Versions of other software used'
	@echo '----------------------------------------------'
	@echo
	@dpkg -l $(DEPENDENCIES)

$(ANALYSIS_STAMP): analysis/process
	cd analysis/ && $(TIME) --format '[analysis] Processing time = %E' ./process $(DB_DBI)
	./analysis/calculate_metrics $(DB_DBI)
	touch $@

$(PROJECTS_STAMP): data/%.stamp : rawdata rawdata/%
	@echo -n "[$*] Starting at "; date
	if ! psql -AtqwlF " " | awk 'NF > 1 { print $$1 }' | grep -q $(DB_NAME); then createdb $(DB_NAME); fi
	cd rawdata/$* && $(TIME) --format '[$*] Processing time = %E' analizo metrics-history -p 4 -f db -b -o $(DB_DBI)
	@echo -n "[$*] Finished at "; date
	touch data/$*.stamp

dbname:
	@echo $(DB_DBI)

rawdata:
	ln -s /home/terceiro/research/rawdata

write-log:
	$(EDITOR) $(EXPERIMENT_LOG)

experiment-clean:
	$(RM) $(DATASETS)
	$(RM) $(ANALYSIS_STAMP)
	$(RM) $(PROJECTS_STAMP)
	$(RM) $(RESULTS_STAMP)

full-clean: clean experiment-clean

# vim: sw=8 ts=8 noexpandtab
