SELECT

  /*********************************************
   context variables
   *********************************************/
  commits.total_eloc                                                            AS  loc,
  commits.total_modules                                                         AS  number_of_modules,

  /*********************************************
   independent variables
   *********************************************/

  (commits.total_eloc - previous.total_eloc)                                    AS  delta_loc,
  sum(first_authorship)                                                         AS  first_authorship,
  sum(deliveries)                                                               AS  deliveries,
  sum(acceptances)                                                              AS  acceptances,
  metrics.experience                                                            AS  experience,

  (
    avg(
      case
        when first_authorship = 1 OR deliveries > acceptances
          then 1
        else 0
      end
    )
  )                                                                             AS  degree_of_knowledge,

  (
    select count(*)
    from commits_module_versions
    where commit_id = commits.id
    and modified = 1
  )                                                                             AS changed_modules,
  (
    select count(*)
    from commits_module_versions
    where commit_id = commits.id
    and added = 1
  )                                                                             AS added_modules,

  /*********************************************
   dependent variables
   *********************************************/
  metrics.sc_system                                                             AS  sc_system,
  metrics.sc_system - metrics.sc_system_before                                  AS  delta_sc_system,
  metrics.sc_subsystem - metrics.sc_subsystem_before                            AS  delta_sc_subsystem,

  commits.developer_id                                                          AS  developer_id,
  commits.date                                                                  AS  date,
  commits.project_id                                                            AS  project_id
FROM
  commits
JOIN
  commits previous ON previous.id = commits.previous_commit_id
JOIN
  authorship ON authorship.commit_id = commits.id
JOIN
  metrics ON metrics.commit_id = commits.id
GROUP BY
  commits.id, previous.id, metrics.commit_id
ORDER BY
  commits.date
;
