/* commits in which the developer had no previous authorship whatsoever
 * over the changed files*/
SELECT *
FROM commits
WHERE
(
  SELECT sum(first_authorship+deliveries)
  FROM doa
  WHERE commit_id = commits.id
) = 0;

/* commits in which the developer had no previous authorship in at least
 * one file changed */
SELECT *
FROM commits
WHERE
  EXISTS(
    SELECT 1
    FROM doa
    WHERE
      commit_id = commits.id
      AND (first_authorship = 0 OR deliveries = 0)
  );


/* number of previous first_authorships, deliveries and acceptances over the
 * files changed by a given commit */
SELECT
  (select sum(first_authorship) from doa where commit_id = commits.id) as first_authorship,
  (select sum(deliveries) from doa where commit_id = commits.id) as deliveries,
  (select sum(acceptances) from doa where commit_id = commits.id) as acceptances,
  id
FROM commits;
