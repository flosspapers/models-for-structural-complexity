## Definitions ##

Degree of knowlegde is given by DOK = f(DOA) = f'(FA, DL, AC)

Structural complexity for a module is given by CBO × LCOM₄=. Structural
complexity for a project is given by the average structural complexity
of its modules and denoted by SC.

ΔSC is the variation in structural complexity caused by a commit. When
ΔSC > 0, the commits made the code more complex. When ΔSC < 0, the
commit made the code less complex.

## Goals of the paper ##

* Test whether degree of knowledge has an influence over the structural
  complexity variation caused by commits.
* Study what other factors might influence structural complexity
  variation.
* Figure out whether SC really captures a valid notion of "complexity".

## Quantitative Study Design ##

* Data extraction: process the repositories and calculate metrics for
  each commit that changes source code.
* Build a linear regression for each project with DOK as independent
  variable and ΔSC as dependent variable; test whether DOK influences
  ΔSC in any way.

Questions:

* FA,DL,AC have values for combinations (time,developer,module), as in
  "when Jane did that commit yesterday changing modules A, B, and C, she
  had FA=1,DL=2,AC=3 on module A, FA=0,DL=2,AC=0 on module B, and did
  not have touched C before". How can we compose the different DOK
  variables over each module to create a single DOK value for the entire
  commit? Do we need to?

  analyze both the max    \
  and the average average / with the weights if the current approach
                            does not work

you would expect that

characterizing the projects in terms of the DOA and ΔSC of commits is
probably more interesting than trying to find whether those things
always go in the same direction.

## Qualitative Study Design ##

The qualitative study consists of looking in detail at a reasonable
number of those commits that were processed in the quantiative study.
The main questions we expect to answer are:

RQ1: what are the reasons, goals and circumstances involved in commits
that cause variation in the structural complexity of a project?

RQ2: are the reasons, goals and circumstances different among commits
with different amounts of structural complexity variation?

RQ3: does the SC metric really captures important aspects of changes?
-> can SC be used to predict which commits should have been reviewed,
because they might introduce problems ...

RQ4: does the degree of authorship ...

what makes one commit more complicated than another? maybe it's for
reasons completely different than SC.

which commits introduced defects?

what changes need oversight? by complexity? by expertise? how the
developers perceive which changes need to be reviewed more than the
others?

which code has been reviewed, and which hasn't?

what code should be reviewed?

high ΔSC and low DOA -> defects (naively). is that the case?

Data collection approach: one of the outputs of the quantitative study
is the value of ΔSC for each commit. We then pick, for each project, 6
commits in the following way: we order them by ΔSC so that they will be
spread over a range like the one below.

 ↓      ↓      ↓     ↓     ↓ ↓     ↓     ↓      ↓      ↓
 ◀――――――――――――――――――――――――――◆――――――――――――――――――――――――――▶
min                        0                          max

We then select the following commits, marked with the arrows in the
above "figure":

* the commit with the smallest ΔSC value
* the median commit with ΔSC < 0
* the commit with largest ΔSC among the ones with ΔSC < 0
* the commit with the smallest ΔSC among the ones with and ΔSC > 0
* the median commit with  ΔSC > 0
* the commit with the largest ΔSC value

look at 10 commits

For each commits, we will look at the following data:

* Dependency graph before and after the commit
* Commit log message and related bug tracker issues, if any
* List of files changed

* did this commit introduce a defect?
* was this commit reviewed?
* DOA, ΔSC

look at from the following viewpoint: how do we tell which commits
should have more priority? if the developer does not have time to look
at all commits, which ones should he look at?

Questions:

* what do we mean by "capture important aspects"? How do we evaluate
  whether SC really makes sense, based on the commits that will be
  studied? One -- maybe crazy -- idea I had was make a small
  experiment: we ask some volunteers to order the selected commits
  from the one that makes the code less complex to the one that makes
  the code more complex, and them compare these rating with the
  ordering provided by the ΔSC metrics.  If ΔSC orders the commits in
  the same way that developers do, then it can be considered a
  "reasonable" metric.
  * we can also ask whether they think that each change needed some
    review, in a range from 0 to 5
* How exactly can we answer the questions after looking closer at these
  commits? (I probably need to educate myself a little more on
  qualitative studies ...)

capture important aspects = can be used to tell whether a change should
be reviewed (or given priority for review) in detriment of others.

TODO:

* determine list of commits to look at
* data extraction form for commits
* look at http://www.st.cs.uni-saarland.de/softevo/bug-data/eclipse/, at
  least to use as reference. OR MAYBE THAY ALREADY HAVE DATA I CAN USE!
* look also: http://www.st.cs.uni-saarland.de/ibugs/
