\section{Background}

\subsection{Authorship and Degree of Knowledge}
\label{sec:background:authorship}

\cite{fritz2010}

\subsection{Structural Complexity}
\label{sec:background:structural-complexity}

\cite{darcy2005}

\subsection{Definitions}
\label{sec:background:definitions}

Let $c$ be a commit, and $p(c)$ the commit before $c$. Let $Ch(c)$ be
the set of modules changed by commit $c$. Let $CBO(m,c)$ and
$LCOM_4(m,c)$ be respectively the values of the CBO and LCOM4 metrics
for module $m$ aftet commit $c$. The structural complexity of module $m$
after commit $c$ is given by $SC(m,c)$, defined as:

\[
SC(m,c) = CBO(m,c) \times LCOM_4(m,c)
\]

The average structural complexity of a set of modules $s$ after
commit $c$ is given by $ASC(s,c)$, which is defined as follows.

\[
ASC(s,c) = \frac{
  \displaystyle\sum_{m \in s} SC(m,c)
}{
  |s|
}
\]

The preceding average structural complexity of the set of modules
\emph{changed} by a
commit $c$ is given by $PASC(c)$, and the following average structural
complexity is given by $FASC(c)$. They are defined as follows

\begin{align*}
PASC(c) & = ASC(Ch(c), p(c)) \\
FASC(c) & = ASC(Ch(c), c) \\
\end{align*}

The relative structural complexity variation caused by commit $c$ is
then given by $RSCV(c)$ and defined as:

\[
RSCV(c) = \frac{
FASC(c) - PASC(c)
}{
PASC(c)
}
\]

Since both $PASC(c)$ are $FASC(c)$ are non-negative real numbers, by
definition $RSCV(c)$ will be a real number between $-1$ and $+\infty$.
In special, if the commit causes no variation in structural complexity,
then $FASC(c) = PASC(c)$, and $RSCV(c) = 0$.


In this work, we use the three authorship components proposed by Fritz
\emph{et al} to determine the degree of knowledge of a developer $d$
over a module $m$, namely $FA$ (first authorship), $DL$ (deliveries),
and $AC$ (acceptances).

A developer $d$ has previous knowledge of a module $m$ if she is the original
author of the module ($FA = 1$), or if it has made more changes to the
module than other developers ($DL > AC$) at the point in time. This is
represented by the $k$ function:

\[
  k(d,m) =
  \left\{
  \begin{array}{l}
    \text{$1$, if $FA(d,m) = 1$ or $DL(d,m) > AC(d,m)$} \\
    \text{$0$, otherwise}
  \end{array}
  \right.
\]

Given $dev(c)$ to be the developer who made a commit $c$, the Degree of
Knowledge a developer had over the modules changed by a commit $c$ is
then given by the proportion of modules over which the developer had
previous knowledge:

\[
DOK'(c) = \frac{
  \displaystyle\sum_{m \in Ch(c)} k(dev(c),m)
}{
  |Ch(c)|
}
\]

For every commit $c$, $DOK'(c)$ ranges from 0 (the developer had no
previous knowledge over the changes modules at all) to 1 (the developer
had previous knowledge over all modules changed).
